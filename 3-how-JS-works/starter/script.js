///////////////////////////////////////
// Lecture: Hoisting

/*
calculateAge(1990);  //function declarations are hoisted, so it works
//retirement(1990);    //expressions are NOT hoisted, so it fails

function calculateAge(year) {
    console.log(2018 - year);
}


var retirement = function (year) {
    console.log(65 - (2018 - year));
}


// variables
console.log(age);
var age = 23;
console.log(age);


function foo() {
    console.log(age); //undefined, because the age variable form this context is hoisted
    var age = 65;
    console.log(age);
}

foo();
console.log(age);

*/





///////////////////////////////////////
// Lecture: Scoping


// First scoping example

/*
var a = 'Hello!';
first();

function first() {
    var b = 'Hi!';
    second();

    function second() {
        var c = 'Hey!';
        console.log(a + b + c);
    }
}
*/



// Example to show the differece between execution stack and scope chain

/*
var a = 'Hello!';
first();

function first() {
    var b = 'Hi!';
    second();

    function second() {
        var c = 'Hey!';
        third()
    }
}

function third() {
    var d = 'John';
    console.log(a + d);
}

*/


///////////////////////////////////////
// Lecture: The this keyword

//console.log(this);

calculateAge(1985);

function calculateAge(year) {
    console.log(2020 - year);
    console.log(this);
}

var john = {
    name: 'John',
    yearOfBirth: 1985,
    calculateAge: function(year) {
        console.log(this);
        console.log(2020 - this.yearOfBirth);

        /*
        function innerFunction() {
            console.log(this); //this refers to global window object because the function is not defined as a method of the object
        }
        innerFunction();
        */
    }
};

john.calculateAge();


var mike = {
    name: 'Mike',
    yearOfBirth: 1984
};

mike.calculateAge = john.calculateAge;
mike.calculateAge(mike.yearOfBirth);




