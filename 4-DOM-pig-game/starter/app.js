/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, 
	winningScore,
	roundScore,
	combinedRoll, 
	activePlayer, 
	gamePlaying;

init();


function init() {

	scores = [0,0];
	roundScore = 0;
	activePlayer = 0;
	gamePlaying = true;
	winningScore = document.getElementById('playto-score').value;

	//reset interface
	document.getElementById('name-0').textContent = 'Player 1';
	document.getElementById('name-1').textContent = 'Player 2';
	document.querySelector('.player-0-panel').classList.remove('active');
	document.querySelector('.player-1-panel').classList.remove('active');
	document.querySelector('.player-0-panel').classList.add('active');
	document.querySelector('.player-0-panel').classList.remove('winner');
	document.querySelector('.player-1-panel').classList.remove('winner');

	//hide dice
	document.querySelector('.dice-0').style.display = 'none';
	document.querySelector('.dice-1').style.display = 'none';

	//set scores to 0
	document.getElementById('score-0').textContent = '0';
	document.getElementById('score-1').textContent = '0';
	document.getElementById('current-0').textContent = '0';
	document.getElementById('current-1').textContent = '0';

	document.querySelector('.btn-roll').style.display = 'block';
	document.querySelector('.btn-hold').style.display = 'block';

}


function switchPlayer() {

	roundScore = 0;
	document.getElementById('current-' + activePlayer).textContent = '0';
	document.querySelector('.player-0-panel').classList.toggle('active');
	document.querySelector('.player-1-panel').classList.toggle('active');
	activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;

}

// roll the dice
document.querySelector('.btn-roll').addEventListener('click', function() {
	
	if (gamePlaying) {

		// generate random dice roll number
		var dice1 = Math.floor(Math.random()*6)+1;
		var dice2 = Math.floor(Math.random()*6)+1;
		combinedRoll = dice1 + dice2;

		// display the result of the dice roll
		var dice1Dom = document.querySelector('.dice-0');
		var dice2Dom = document.querySelector('.dice-1');
		dice1Dom.style.display = 'block';
		dice1Dom.src = 'dice-' + dice1 + '.png';
		dice2Dom.style.display = 'block';
		dice2Dom.src = 'dice-' + dice2 + '.png';

		// update the round score if the rolled number is not 1
		if (dice1 !== 1 && dice2 !== 1 ) {
			if (dice1 === 1 && dice2 === 1) {
				scores[activePlayer] = 0;
				document.getElementById('score-'+activePlayer).textContent = 0;
				switchPlayer();
			}
			else {
				// add score
				roundScore += combinedRoll;
				document.querySelector('#current-'+activePlayer).textContent = roundScore;
			}	
		}
		else {
			// switch to next player and reset round score
			switchPlayer();
		}
	}

});


// hold the player score
document.querySelector('.btn-hold').addEventListener('click', function(){

	if (gamePlaying) {

		// Add current core to player global score
		scores[activePlayer] += roundScore;

		// Update the UI
		document.getElementById('score-'+activePlayer).textContent = scores[activePlayer];
		
		// Check to see if the player won the game
		if (scores[activePlayer] >= winningScore) {
			document.getElementById('name-'+activePlayer).textContent = 'Winner!';
			document.querySelector('.player-'+activePlayer+'-panel').classList.remove('active');
			document.querySelector('.player-'+activePlayer+'-panel').classList.add('winner');
			document.querySelector('.dice-0').style.display = 'none';
			document.querySelector('.dice-1').style.display = 'none';
			document.querySelector('.btn-roll').style.display = 'none';
			document.querySelector('.btn-hold').style.display = 'none';
			gamePlaying = false;
		}
		else {
			switchPlayer();
		}
	
	}

});

// restart the game
document.querySelector('.btn-new').addEventListener('click', init);

