// Function constructor

/*
var Person = function(name, yearOfBirth, job) {
	this.name = name;
	this.yearOfBirth = yearOfBirth;
	this.job = job;
}


Person.prototype.calculateAge = function() {
	console.log(2020 - this.yearOfBirth);
}

Person.prototype.lastName = 'Smith';


var john = new Person('John', 1990, 'teacher');
var jane = new Person('Jane', 1969, 'designer');
var mark = new Person('Mark', 1948, 'retired');

john.calculateAge();
jane.calculateAge();
mark.calculateAge();

console.log(john.lastName);
console.log(jane.lastName);
console.log(mark.lastName);

*/


/*
// Object.create
var personProto = {
	calculateAge: function() {
		console.log(2020 - this.yearOfBirth);
	}
};

var john = Object.create(personProto);
john.name = 'John';
john.yearOfBirth = 1990;
john.job = 'teacher';

var jane = Object.create(personProto, 
	{
		name: { value: 'Jane' },
		yearOfBirth: { value: 1969 },
		job: { value: 'designer' }
	}
);
*/


// Primitives as Objects
// Primitives
/*
var a = 23;
var b = a;
a = 46;
console.log(a,b);


//Objects 
var obj1 = {
	name: 'John',
	age: 26
};

var obj2 = obj1;
obj1.age = 30;

console.log(obj1.age);
console.log(obj2.age);

// Functions
var age = 27;
var obj = {
	name: 'Jonas',
	city: 'Lisbon'
};

function change(a, b) {
	a = 30,    // primitive is not updated
	b.city = 'San Francisco'  // object is updated
}


change(age, obj);

console.log(age);
console.log(obj.city);

*/


// Passing functions as arguments
/*
var years = [1990, 1965, 1937, 2005, 1998];

function arrayCalc (arr, fn) {
	var arrRes = [];
	for (var i = 0; i < arr.length; i++) {
		arrRes.push(fn(arr[i]));
	}
	return arrRes;
}


function calculateAge(el) {
	return 2020 - el;
}

function isFullAge(el) {
	return el >= 18;
}

function maxHeartRate(el) {

	if (el >= 18 && el <= 81) {
		return Math.round(206.9 - 0.67 * el);
	}
	else {
		return -1;
	}
	
}

var ages = arrayCalc(years, calculateAge);
var fullAges = arrayCalc(ages, isFullAge);
var maxHeartRates = arrayCalc(ages, maxHeartRate);


console.log(ages);
console.log(fullAges);
console.log(maxHeartRates);
*/


///////////////////////////////////////////
// Functions returning functions
/*
function interviewQuestion(job) {
	if (job === 'designer') {
		return function(name) {
			console.log(name + ', can you please explain what UX design is?');
		}
	} else if (job === 'teacher') {
		return function(name) {
			console.log(name + ', which subject do you teach?');
		}
	} else {
		return function(name) {
			console.log('Hello ' + name + '. What do you do?');
		}
	}
}

var teacherQuestion = interviewQuestion('teacher');
var designerQuestion = interviewQuestion('designer');

teacherQuestion('John');
designerQuestion('John');
designerQuestion('Jane');

interviewQuestion('teacher')('Mark');
*/


///////////////////////////////////////////
// Immediately invoked function expressions - IIFE
/*
function game() {
	var score = Math.random() * 10;
	console.log(score >= 5);
}

game();


(function() {
	var score = Math.random() * 10;
	console.log(score >= 5);
})();



(function(goodLuck) {
	var score = Math.random() * 10;
	console.log(score >= 5 - goodLuck);
})(5);

*/

//////////////////////////////////////////
// Closures
/*
function retirement(retirementAge) {
	var a = ' years left until retirement.';
	return function(yearOfBirth) {
		var age = 2016 - yearOfBirth;
		console.log((retirementAge-age) + a);
	}
}

var retirementUS = retirement(66);
var retirementGermany = retirement(65);
var retirementIceland = retirement(67);

retirementUS(1990);
retirementGermany(1990);
retirementIceland(1990);


function interviewQuestion(job) {
	return function(name) {
		if (job === 'designer') {
			console.log(name + ', can you please explain what UX design is?');
		} else if (job === 'teacher') {
			console.log(name + ', which subject do you teach?');
		} else {
			console.log('Hello ' + name + '. What do you do?');
		}
	}
}

interviewQuestion('teacher')('John');
interviewQuestion('teacher')('Matt');
interviewQuestion('designer')('Jane');
interviewQuestion('retired')('Phil');

*/

//////////////////////////////////////////
// Bind, Call, and Apply
/*
var john = {
	name: 'John',
	age: 26,
	job: 'teacher',
	presentation: function(style, timeOfDay) {

		if (style === 'formal') {
			console.log('Good ' + timeOfDay + ', ladies and gentlemen. I\'m ' + this.name + '. I am a ' + this.job + ' and I am ' + this.age + ' years old.');
		}
		else if (style === 'friendly') {
			console.log('Hey!  What\'s up? I\'m ' + this.name + '. I am a ' + this.job + ' and I am ' + this.age + ' years old. Have a nice ' + timeOfDay + '.');		
		}
	}
}

var emily = {
	name: 'Emily',
	age: 35,
	job: 'designer',
}


//john.presentation('formal', 'morning');

//john.presentation('friendly', 'morning');

//	john.presentation.call(emily, 'friendly', 'afternoon');

//john.presentation.apply(emily, ['friendly', 'afternoon']);

var johnFriendly = john.presentation.bind(john, 'friendly');
var emilyFormal = john.presentation.bind(emily, 'formal');

johnFriendly('morning');
johnFriendly('night');
emilyFormal('morning');
emilyFormal('night');

*/

/*

var years = [1990, 1965, 1937, 2005, 1998];

function arrayCalc (arr, fn) {
	var arrRes = [];
	for (var i = 0; i < arr.length; i++) {
		arrRes.push(fn(arr[i]));
	}
	return arrRes;
}


function calculateAge(el) {
	return 2016 - el;
}

function isFullAge(limit, el) {
	return el >= limit;
}

var ages = arrayCalc(years, calculateAge);
var fullJapan = arrayCalc(ages, isFullAge.bind(this, 20));
var fullUS = arrayCalc(ages, isFullAge.bind(this, 18));

console.log(ages);
console.log(fullJapan);
console.log(fullUS);

*/


//////////////////////////////////////////
// Coding Challenge

(function() {
	

	var Question = function(questionText, answers, correctAnswerId){
		this.questionText = questionText;
		this.answers = answers;
		this.correctAnswer = correctAnswerId;
	};

	Question.prototype.printQuestion = function() {
		console.log(this.questionText);
		for (var i =0; i < this.answers.length; i++) {
			console.log(i + ' - ' + this.answers[i]);
		}
	};

	Question.prototype.getAnswer = function() {
		var userAnswer = prompt('What do you think is the right answer?');
		if (userAnswer == this.correctAnswer) {
			console.log('That\'s the right answer!');
		}
		else {
			console.log('Womp womp womp...');
		}
	};


	var Question1 = new Question('What is your favorite movie?', ['The Goonies', 'Back to the Future', 'Unforgiven'], 2);
	var Question2 = new Question('What is best?', ['Spahetti', 'Pizza', 'Steak'], 1);
	var Question3 = new Question('When?', ['Never', 'Always', 'Tomorrow'], 1);

	var quizQuestions = [Question1, Question2, Question3]

	function askQuestion(questions) {
		var questionNumber = Math.round(Math.random() * (questions.length - 1));
		questions[questionNumber].printQuestion();
		questions[questionNumber].getAnswer();
	}

	askQuestion(quizQuestions);


})();















